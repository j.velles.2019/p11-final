import sys
import os
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
import datetime

def log_message(message):
    timestamp = datetime.datetime.now().strftime("%Y%m%d%H%M%S%f")[:-3]
    log_entry = f"{timestamp} {message}"
    sys.stderr.write(log_entry + "\n")

import argparse
import asyncio
import json
import logging
import platform
import jinja2
from aiohttp import web
from aiortc import RTCSessionDescription
from aiortc.contrib.media import MediaPlayer, MediaRelay
from aiortc.rtcrtpsender import RTCRtpSender

ROOT = os.path.dirname(__file__)

relay = None
webcam = None
received_list = []
received_answer = ""
client_protocol = None
remote_addr = ""
titles = []
descriptions = []

def create_local_tracks(play_from, decode):
    global relay, webcam

    if play_from:
        player = MediaPlayer(play_from, decode=decode)
        return player.audio, player.video
    else:
        options = {"framerate": "30", "video_size": "640x480"}
        if relay is None:
            if platform.system() == "Darwin":
                webcam = MediaPlayer("default:none", format="avfoundation", options=options)
            elif platform.system() == "Windows":
                webcam = MediaPlayer("video=Integrated Camera", format="dshow", options=options)
            else:
                webcam = MediaPlayer("/dev/video0", format="v4l2", options=options)
            relay = MediaRelay()
        return None, relay.subscribe(webcam.video)

def force_codec(pc, sender, forced_codec):
    kind = forced_codec.split("/")[0]
    codecs = RTCRtpSender.getCapabilities(kind).codecs
    transceiver = next(t for t in pcs.getTransceivers() if t.sender == sender)
    transceiver.setCodecPreferences([codec for codec in codecs if codec.mimeType == forced_codec])

async def index(request):
    await initialize_client()
    template = jinja2.Template(open(os.path.join(ROOT, "index.html")).read())
    context = {'videos': received_list, 'titles': titles, 'descriptions': descriptions}
    return web.Response(text=template.render(context), content_type='text/html')

async def list_videos(request):
    return web.json_response(received_list)

async def javascript(request):
    content = open(os.path.join(ROOT, "client.js"), "r").read()
    return web.Response(content_type="application/javascript", text=content)

async def offer(request):
    log_message('Mensaje de oferta SDP del navegador recibido')
    params = await request.json()
    log_message('Mensaje de oferta SDP del navegador enviado a (127.0.0.1, 9090)')
    if 'sdp' in params and 'type' in params:
        offer = RTCSessionDescription(sdp=params["sdp"], type=params["type"])
        video_elegido = "Name:" + params["video"]
        client_protocol.transport.sendto(video_elegido.encode())
        client_protocol.transport.sendto(json.dumps(offer.__dict__).encode())
        await wait_received_answer()
        global received_answer
        answer = json.loads(received_answer)
        sdp = answer["sdp"]
        log_message('Mensaje de respuesta SDP al navegador enviado')
        received_answer = ""
        return web.Response(
            content_type="application/json",
            text=json.dumps({"sdp": sdp, "type": "answer"}),
        )
    else:
        print("Error: 'sdp' y/o 'type' no existen en params")
        return web.Response(text='Error: "sdp" y/o "type" no existen en params', status=400)

pcs = set()

async def on_shutdown(app):
    coros = [pc.close() for pc in pcs]
    await asyncio.gather(*coros)
    pcs.clear()

async def initialize_client():
    loop = asyncio.get_running_loop()
    on_con_lost = loop.create_future()
    message = "LISTA"
    global client_protocol
    client_protocol = EchoClientProtocol(message, on_con_lost)
    await loop.create_datagram_endpoint(lambda: client_protocol, remote_addr=remote_addr)
    await wait_received_list()

class EchoClientProtocol:
    def __init__(self, message, on_con_lost):
        self.message = message
        self.on_con_lost = on_con_lost
        self.transport = None

    def connection_made(self, transport):
        self.transport = transport
        log_message('Mensaje de peticion del listado de videos enviado a (127.0.0.1, 9090)')
        self.transport.sendto(self.message.encode())

    def datagram_received(self, data, addr):
        if data.decode().split('"')[-2] == "answer":
            log_message('Mensaje de respuesta SDP al navegador recibido de ' + str(addr))
            global received_answer
            print("Received:", data.decode())
            received_answer = data.decode()

        if "video" in data.decode():
            log_message('Mensaje de listado de videos recibido de ' + str(addr))
            global received_list, titles, descriptions
            print("Received:", json.loads(data.decode()))
            n = 0
            for video in json.loads(data.decode()):
                received_list.append(str(json.loads(video).keys()).split("'")[1])
                titles.append(json.loads(video)[received_list[n]]["Titulo"])
                descriptions.append(json.loads(video)[received_list[n]]["Descripcion"])
                n += 1

    def error_received(self, exc):
        print('Error received:', exc)

    def connection_lost(self):
        print("Connection closed")
        self.on_con_lost.set_result(True)

async def wait_received_list():
    while received_list == "":
        await asyncio.sleep(1)
    await asyncio.sleep(1)

async def wait_received_answer():
    while received_answer == "":
        await asyncio.sleep(1)

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("http_port", type=int, help="TCP port for HTTP requests")
    parser.add_argument("signal_ip", help="Signaling server IP address")
    parser.add_argument("signal_port", type=int, help="Signaling server port")

    args = parser.parse_args()
    global remote_addr
    remote_addr = (args.signal_ip, args.signal_port)
    logging.basicConfig(level=logging.INFO)
    ssl_context = None
    log_message("Comenzando")
    app = web.Application()
    app.on_shutdown.append(on_shutdown)
    app.router.add_get("/", index)
    app.router.add_get("/list", list_videos)  # Añadido endpoint /list
    app.router.add_get("/client.js", javascript)
    app.router.add_post("/offer", offer)
    web.run_app(app, host="0.0.0.0", port=args.http_port, ssl_context=ssl_context)

if __name__ == "__main__":
    main()
