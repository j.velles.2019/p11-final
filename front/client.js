var pc = null;

function negotiate(videoName) {
    pc.addTransceiver('video', { direction: 'recvonly' });
    pc.addTransceiver('audio', { direction: 'recvonly' });
    return pc.createOffer().then((offer) => {
        return pc.setLocalDescription(offer);
    }).then(() => {
        // wait for ICE gathering to complete
        return new Promise((resolve) => {
            if (pc.iceGatheringState === 'complete') {
                resolve();
            } else {
                const checkState = () => {
                    if (pc.iceGatheringState === 'complete') {
                        pc.removeEventListener('icegatheringstatechange', checkState);
                        resolve();
                    }
                };
                pc.addEventListener('icegatheringstatechange', checkState);
            }
        });
    }).then(() => {
        var offer = pc.localDescription;
        saveSDPToFile('offer', offer.sdp);
        return fetch('/offer', {
            body: JSON.stringify({
                sdp: offer.sdp,
                type: offer.type,
                video: videoName
            }),
            headers: {
                'Content-Type': 'application/json'
            },
            method: 'POST'
        });
    }).then((response) => {
        return response.json();
    }).then((answer) => {
        saveSDPToFile('answer', answer.sdp);
        return pc.setRemoteDescription(answer);
    }).catch((e) => {
        alert(e);
    });
}

function saveSDPToFile(type, sdp) {
    const blob = new Blob([sdp], { type: 'application/octet-stream' });
    const url = URL.createObjectURL(blob);
    const a = document.createElement('a');
    a.style.display = 'none';
    a.href = url;
    a.download = `${type}_sdp_${Date.now()}.txt`;
    document.body.appendChild(a);
    a.click();
    window.URL.revokeObjectURL(url);
}

function start(videoName) {
    var config = {
        sdpSemantics: 'unified-plan'
    };

    if (document.getElementById('use-stun').checked) {
        config.iceServers = [{ urls: ['stun:stun.l.google.com:19302'] }];
    }

    pc = new RTCPeerConnection(config);

    // connect audio / video
    pc.addEventListener('track', (evt) => {
        if (evt.track.kind == 'video') {
            document.getElementById('video').srcObject = evt.streams[0];
        } else {
            document.getElementById('audio').srcObject = evt.streams[0];
        }
    });

    // Get the correct elements using the dynamic ID
    const startButton = document.getElementById(`start ${videoName}`);
    const stopButton = document.getElementById(`stop ${videoName}`);

    // Debugging logs
    console.log("Start button element:", startButton);
    console.log("Stop button element:", stopButton);

    // Ensure elements are not null
    if (startButton && stopButton) {
        startButton.style.display = 'none';
        negotiate(videoName);
        stopButton.style.display = 'inline-block';
    } else {
        console.error("Start or Stop button not found in the DOM");
    }
}

function stop(videoName) {
    const stopButton = document.getElementById(`stop ${videoName}`);
    const startButton = document.getElementById(`start ${videoName}`);

    // Ensure elements are not null
    if (stopButton && startButton) {
        stopButton.style.display = 'none';
        startButton.style.display = 'inline-block';
    } else {
        console.error("Start or Stop button not found in the DOM");
    }

    // close peer connection
    setTimeout(() => {
        pc.close();
    }, 500);
}
