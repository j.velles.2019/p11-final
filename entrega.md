# ENTREGA CONVOCATORIA JUNIO
**Nombre del Estudiante:** Javier Velles Sánchez
**Correo de la Universidad:** j.velles.2019@alumnos.urjc.es

---

## Parte básica
Este programa implementa un sistema de transmisión de video en tiempo real utilizando WebRTC. La parte básica consta de todos los programas requeridos en el enunciado del proyecto.

### Descripción del Funcionamiento
Primero, el sistema de señalización se inicia en el puerto especificado y se configuran los cuatro transmisores de video que transmiten diferentes videos de automóviles.

1. **Iniciar el Servidor de Señalización:**
   - El servidor de señalización se inicia en el puerto 8080.
   - Este servidor recibe y reenvía mensajes de descripción de sesión (SDP) entre los clientes y los servidores de video para establecer la conexión.
   - **Comando:**
     ```bash
     python3 signalling.py 8080
     ```

2. **Iniciar los Servidores de Video:**
   - Se inician cuatro servidores de video, cada uno sirviendo un video diferente (Audi, BMW, Mercedes, Tesla).
   - Los servidores se registran en el servidor de señalización indicando su nombre y el archivo de video que van a servir.
   - Durante la inicialización, los transmisores envían mensajes de registro al sistema de señalización, proporcionando su información.
   - **Comandos:**
     ```bash
     python3 streamer.py videos/audi.mp4 127.0.0.1 8080
     python3 streamer.py videos/bmw.mp4 127.0.0.1 8080
     python3 streamer.py videos/mercedes.mp4 127.0.0.1 8080
     python3 streamer.py videos/tesla.mp4 127.0.0.1 8080
     ```

3. **Iniciar el Cliente Web:**
   - El servidor frontal se configura en el puerto 8081 y se comunica con el servidor de señalización.
   - El cliente web permite a los usuarios ver la lista de videos disponibles y seleccionar cuál reproducir.
   - **Comando:**
     ```bash
     python3 front.py 8081 127.0.0.1 8080
     ```

4. **Interfaz Web y Selección de Video:**
   - Los usuarios acceden a la interfaz web desde un navegador y ven la lista de videos disponibles.
   - Al hacer clic en el botón "Reproducir" para un video, el cliente web envía una oferta SDP al servidor de señalización.
   - El servidor de señalización reenvía la oferta al servidor de video correspondiente.
   - El servidor de video procesa la oferta y genera una respuesta SDP que se envía de vuelta al cliente web.
   - El cliente web establece la conexión WebRTC y comienza a recibir y reproducir el video en tiempo real.

### Comentarios
- **Limitación:** SOLO FUNCIONA EN EL NAVEGADOR GOOGLE CHROME.
- El proceso de registro y transmisión se realiza de manera eficiente.

---

## Parte adicional
### Descripción de Funcionalidades Adicionales
Además de la parte básica, se han implementado las siguientes funcionalidades adicionales:

- **Información adicional para los ficheros:**
  - Cuando los streamers se registran, envían al servidor de señalización información adicional.
  - Esta información incluye un título y un resumen en texto del fichero.
  - La información se almacena en el directorio del servidor de señalización.
  - El servidor frontal solicita esta información al servidor de señalización para mostrarla en la interfaz web a los usuarios.


---

### Enlace al Video de Demostración del Proyecto
* https://youtu.be/qIw0pp1TI0Q
